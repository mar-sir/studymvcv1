package com.hcl.studymvphcl.login.presenter.domain;

/**
 * Created by Administrator on 2016/1/22 0022.
 */
public interface LoginView {
    void showProgress();

    void hideProgress();

    void setUsernameError();

    void setPasswordError();

    void navigateToHome();
}
