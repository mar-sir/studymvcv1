package com.hcl.studymvphcl.login.presenter;


import com.hcl.studymvphcl.login.presenter.domain.LoginInteractor;
import com.hcl.studymvphcl.login.presenter.domain.LoginPresenter;
import com.hcl.studymvphcl.login.presenter.domain.LoginView;
import com.hcl.studymvphcl.login.presenter.domain.OnLoginFinishedListener;

/**
 * Created by Administrator on 2016/1/22 0022.
 */
public class LoginPresenterImpl implements LoginPresenter, OnLoginFinishedListener {
    private LoginView loginView;
    private LoginInteractor loginInteractor;

    public LoginPresenterImpl(LoginView loginView) {
        this.loginView = loginView;
        loginInteractor = new LoginInterzctorImpl();
    }


    @Override
    public void validateCredentials(String username, String password) {
        if (loginView != null) {
            loginView.showProgress();
        }
        loginInteractor.login(username, password, this);
    }

    @Override
    public void onDestory() {
        loginView = null;
    }


    @Override
    public void onUsernameError() {
        if (loginView != null) {
            loginView.setUsernameError();
            loginView.hideProgress();
        }
    }

    @Override
    public void onPasswordError() {
        if (loginView != null) {
            loginView.setPasswordError();
            loginView.hideProgress();
        }
    }

    @Override
    public void onSuccess() {
        if (loginView != null) {
            loginView.navigateToHome();
        }
    }

}
