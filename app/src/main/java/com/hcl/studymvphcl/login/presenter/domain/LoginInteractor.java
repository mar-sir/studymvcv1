package com.hcl.studymvphcl.login.presenter.domain;

/**
 * Created by Administrator on 2016/1/22 0022.
 */
public interface LoginInteractor {
    void login(String username, String password, OnLoginFinishedListener listener);
}
