package com.hcl.studymvphcl.login.presenter;

import android.os.Handler;
import android.text.TextUtils;

import com.hcl.studymvphcl.login.presenter.domain.LoginInteractor;
import com.hcl.studymvphcl.login.presenter.domain.OnLoginFinishedListener;

/**
 * Created by Administrator on 2016/1/22 0022.
 */
public class LoginInterzctorImpl implements LoginInteractor {
    @Override
    public void login(final String username, final String password, final OnLoginFinishedListener listener) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                boolean error = false;
                if (TextUtils.isEmpty(username)) {
                    listener.onUsernameError();
                    error = true;
                }
                if (TextUtils.isEmpty(password)) {
                    listener.onPasswordError();
                    error = true;
                }
                if (!error) {
                    listener.onSuccess();
                }
            }
        }, 2000);
    }

}
