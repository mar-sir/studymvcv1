package com.hcl.studymvphcl.info.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hcl.studymvphcl.R;
import com.hcl.studymvphcl.info.model.User;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by Administrator on 2016/1/23 0023.
 */
public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyHolder> implements View.OnClickListener {
    private ArrayList<User> lists;
    private LayoutInflater inflater;
    private OnItemClickListener listener;
    private RecyclerView recyclerView;


    public MyAdapter(Context context, ArrayList<User> lists) {
        this.lists = lists;
        inflater = LayoutInflater.from(context);
    }


    public void setListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_user, parent, false);
        MyHolder holder = new MyHolder(view);
        view.setOnClickListener(this);
        return holder;
    }

    public void addAll(Collection<User> users) {
        this.lists.addAll(users);
        notifyDataSetChanged();

    }

    @Override
    public void onBindViewHolder(MyHolder holder, int position) {
        User user = lists.get(position);
        holder.userTxt.setText(user.getName() + "    " + user.getPhone());
    }


    @Override
    public int getItemCount() {
        return lists.isEmpty() ? 0 : lists.size();
    }

    @Override
    public void onClick(View v) {
        int postion = recyclerView.getChildAdapterPosition(v);
        if (listener != null) {
            listener.onItemClickListener(postion);
        }
    }


    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        this.recyclerView = recyclerView;
    }

    class MyHolder extends RecyclerView.ViewHolder {
        TextView userTxt;

        public MyHolder(View itemView) {
            super(itemView);
            userTxt = (TextView) itemView;
        }
    }

    public interface OnItemClickListener {
        void onItemClickListener(int position);
    }
}
