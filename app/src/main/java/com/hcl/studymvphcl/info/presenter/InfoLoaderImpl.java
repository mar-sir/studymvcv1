package com.hcl.studymvphcl.info.presenter;

import android.content.Context;
import android.database.Cursor;
import android.os.Handler;
import android.os.Message;
import android.provider.ContactsContract;

import com.hcl.studymvphcl.info.model.User;
import com.hcl.studymvphcl.info.presenter.domain.InfoLoader;

import java.util.ArrayList;

/**
 * Created by Administrator on 2016/1/23 0023.
 */
public class InfoLoaderImpl implements InfoLoader {


    @Override
    public void loadInfo(final Context context, final Handler handler) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                User user = null;
                ArrayList<User> users = new ArrayList<>();
                Cursor phone=null;
                Cursor cursor = context.getContentResolver()
                        .query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
                int contactIdIndex = 0;
                int nameIndex = 0;
                if (cursor.getCount() > 0) {
                    contactIdIndex = cursor.getColumnIndex(ContactsContract.Contacts._ID);
                    nameIndex = cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);
                }
                while (cursor.moveToNext()) {
                    String contactId = cursor.getString(contactIdIndex);
                    String name = cursor.getString(nameIndex);
                           phone = context.getContentResolver()
                            .query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "=" + contactId,
                                    null, null);
                    int phoneIndex = 0;
                    if (phone.getCount() > 0) {
                        phoneIndex = phone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                    }
                    while (phone.moveToNext()) {
                        String phoneNumber = phone.getString(phoneIndex);
                        user = new User();
                        user.setName(name);
                        user.setPhone(phoneNumber);
                        users.add(user);
                    }
                    phone.close();
                }
                if (!users.isEmpty()) {
                    Message message = Message.obtain();
                    message.obj = users;
                    handler.sendMessage(message);
                }
                if (cursor != null) {
                    cursor.close();
                }
            }
        }).start();
    }

}
