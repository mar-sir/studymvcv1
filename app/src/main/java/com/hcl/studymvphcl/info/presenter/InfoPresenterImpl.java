package com.hcl.studymvphcl.info.presenter;

import android.content.Context;
import android.os.Handler;
import android.os.Message;

import com.hcl.studymvphcl.info.model.User;
import com.hcl.studymvphcl.info.presenter.domain.InfoLoader;
import com.hcl.studymvphcl.info.presenter.domain.InfoPresenter;
import com.hcl.studymvphcl.info.presenter.domain.InfoView;

import java.util.ArrayList;

/**
 * Created by Administrator on 2016/1/23 0023.
 */
public class InfoPresenterImpl implements InfoPresenter {
    private InfoView infoView;
    private InfoLoader infoLoader;
    private Context context;
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (infoView != null) {
                infoView.setAdapter((ArrayList<User>) msg.obj);
            }
        }
    };

    public InfoPresenterImpl(InfoView infoView, Context context) {
        this.infoView = infoView;
        this.context = context;
        infoLoader = new InfoLoaderImpl();
    }

    @Override
    public void onResoume() {
        if (infoView != null) {
            //前台显示UI
            infoView.loadData();
        }
        //后台加载数据
        infoLoader.loadInfo(context, handler);
    }

    @Override
    public void onItemClick(int position) {
        if (infoView != null) {
            infoView.showUser(position);
        }
    }

    @Override
    public void onDestory() {
        infoView = null;
    }

}
