package com.hcl.studymvphcl.info.presenter.domain;

/**
 * Created by Administrator on 2016/1/23 0023.
 */
public interface InfoPresenter {
    void onResoume();

    void onItemClick(int position);

    void onDestory();
}
