package com.hcl.studymvphcl.info.view.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.hcl.studymvphcl.R;
import com.hcl.studymvphcl.info.model.User;
import com.hcl.studymvphcl.info.presenter.InfoPresenterImpl;
import com.hcl.studymvphcl.info.presenter.domain.InfoPresenter;
import com.hcl.studymvphcl.info.presenter.domain.InfoView;
import com.hcl.studymvphcl.info.view.adapter.MyAdapter;

import java.util.ArrayList;

public class InfoActivity extends AppCompatActivity implements InfoView, MyAdapter.OnItemClickListener {
    private RecyclerView recyclerView;
    private InfoPresenter presenter;
    private ProgressBar progressBar;

    private MyAdapter adapter;
    private ArrayList<User> users = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        initView();
        init();

    }

    private void init() {
        presenter = new InfoPresenterImpl(this, this);
        adapter = new MyAdapter(this, users);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    private void initView() {
        recyclerView = (RecyclerView) findViewById(R.id.info_recycle);
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onResoume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestory();
    }

    @Override
    public void loadData() {
        progressBar = new ProgressBar(this);
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void setAdapter(ArrayList<User> users) {
        this.users = users;
        adapter.addAll(users);
        adapter.setListener(this);
    }

    @Override
    public void showUser(int position) {
        Toast.makeText(this, users.get(position).toString(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onItemClickListener(int position) {
        presenter.onItemClick(position);
    }
}
