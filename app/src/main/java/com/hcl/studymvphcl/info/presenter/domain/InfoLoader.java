package com.hcl.studymvphcl.info.presenter.domain;

import android.content.Context;
import android.os.Handler;

/**
 * Created by Administrator on 2016/1/23 0023.
 */
public interface InfoLoader {
    //联系加载完后由 listener回调出去
    void loadInfo(Context context, Handler handler);

}
