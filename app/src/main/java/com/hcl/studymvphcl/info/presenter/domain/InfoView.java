package com.hcl.studymvphcl.info.presenter.domain;

import com.hcl.studymvphcl.info.model.User;

import java.util.ArrayList;

/**
 * Created by Administrator on 2016/1/23 0023.
 */
public interface InfoView {
    void loadData();


    void setAdapter(ArrayList<User> users);


    void showUser(int position);
}
