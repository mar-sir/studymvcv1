package com.hcl.studymvphcl.main.presenter;

import com.hcl.studymvphcl.main.presenter.domain.FindItemsInteractor;
import com.hcl.studymvphcl.main.presenter.domain.MainPresenter;
import com.hcl.studymvphcl.main.presenter.domain.OnFinishedListener;
import com.hcl.studymvphcl.main.presenter.domain.MainView;

import java.util.List;

/**
 * Created by Administrator on 2016/1/22 0022.
 */
public class MainPresenterImpl implements MainPresenter, OnFinishedListener {
    private MainView mainView;
    private FindItemsInteractor findItemsInteractor;


    public MainPresenterImpl(MainView mainView) {
        this.mainView = mainView;
        this.findItemsInteractor = new FindItemsInteractorImpl();
    }

    @Override
    public void onResume() {
        if (mainView != null) {
            mainView.showProgress();
        }
        findItemsInteractor.findItems(this);
    }


    @Override
    public void onItemClicked(int position) {
        mainView.showMessage(String.format("Position %d clicked", position + 1));
    }

    @Override
    public void onDestory() {
        mainView = null;
    }

    @Override
    public void onFinished(List<String> items) {
        if (mainView != null) {
            mainView.setItems(items);
            mainView.hideProgress();
        }
    }


}
