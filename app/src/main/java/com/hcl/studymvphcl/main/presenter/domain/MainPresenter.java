package com.hcl.studymvphcl.main.presenter.domain;

/**
 * Created by Administrator on 2016/1/22 0022.
 */
public interface MainPresenter {

    void onResume();

    void onItemClicked(int position);

    void onDestory();
}
