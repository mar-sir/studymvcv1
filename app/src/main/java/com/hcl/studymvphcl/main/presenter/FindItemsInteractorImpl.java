package com.hcl.studymvphcl.main.presenter;

import android.os.Handler;

import com.hcl.studymvphcl.main.presenter.domain.FindItemsInteractor;
import com.hcl.studymvphcl.main.presenter.domain.OnFinishedListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/1/22 0022.
 */
public class FindItemsInteractorImpl implements FindItemsInteractor {
    @Override
    public void findItems(final OnFinishedListener listener) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                listener.onFinished(createArrayList());
            }
        }, 2000);
    }

    private List<String> createArrayList() {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            list.add("hcl-->" + i);
        }
        return list;
    }
}
