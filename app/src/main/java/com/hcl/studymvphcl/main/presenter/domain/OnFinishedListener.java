package com.hcl.studymvphcl.main.presenter.domain;

import java.util.List;

/**
 * Created by Administrator on 2016/1/22 0022.
 */
public interface OnFinishedListener {
    void onFinished(List<String> items);
}
